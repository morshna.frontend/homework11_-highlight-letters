
let button = document.querySelectorAll('.btn');

document.addEventListener('keydown', function(e){
  button.forEach(item => {
    if(e.key.toUpperCase() === item.dataset.tabName.toUpperCase()){
      item.classList.add('colored');
    } else{
      item.classList.remove('colored');
    }  
  });
})
